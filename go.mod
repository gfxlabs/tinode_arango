module gitlab.com/gfxlabs/tinode_arango

go 1.17

require (
	github.com/arangodb/go-driver v1.2.1
	github.com/tinode/chat v0.18.2
	gitlab.com/gfxlabs/goutil v1.5.0
	gitlab.com/gfxlabs/structs v1.2.0
)

require (
	github.com/arangodb/go-velocypack v0.0.0-20200318135517-5af53c29c67e // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/tinode/snowflake v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
)
