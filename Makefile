APPS = $(shell ls app)
HACKS = $(shell ls hack)

tidy:
	go mod tidy
	echo $(APPS)

apps: tidy
	mkdir -p bin
	for dir in $(APPS); do\
		go build -o ./bin/$$dir.exe ./app/$$dir; \
	done


clean:
	rm -rf bin

docker:
	export DOCKER_BUILDKIT=1
	for dir in $(APPS); do\
		docker build -f $$dir.Dockerfile -t gfxlabs/$$dir .; \
	done

HACKS = $(shell ls hack)

hacks: tidy
	mkdir -p bin
	for dir in $(HACKS); do\
		go run ./hack/$$dir; \
	done
